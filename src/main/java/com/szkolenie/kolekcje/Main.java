package com.szkolenie.kolekcje;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();
        Set<Integer> hashSet = new HashSet<>();
        Set<Integer> linkedHashSet = new LinkedHashSet<>();
        Set<Integer> treeSet = new TreeSet<>();

        int number;
        long start;
        do {
            System.out.println("\nPodaj kolejna liczbe:");
            number = sc.nextInt();
            if (number == 0) break;

            start = System.nanoTime();
            arrayList.add(number);
            System.out.print(" AL: " + (System.nanoTime() - start));

            start = System.nanoTime();
            linkedList.add(number);
            System.out.print(" LL: " + (System.nanoTime() - start));

            start = System.nanoTime();
            hashSet.add(number);
            System.out.print(" HS: " + (System.nanoTime() - start));

            start = System.nanoTime();
            linkedHashSet.add(number);
            System.out.print(" LHS: " + (System.nanoTime() - start));

            start = System.nanoTime();
            treeSet.add(number);
            System.out.print(" TS: " + (System.nanoTime() - start));
        } while (number != 0);

        System.out.println("ArrayList: " + arrayList);
        System.out.println("LinkedList: " + linkedList);
        System.out.println("HashSet: " + hashSet);
        System.out.println("LinkedHashSet: " + linkedHashSet);
        System.out.println("TreeSet: " + treeSet);
    }
}
