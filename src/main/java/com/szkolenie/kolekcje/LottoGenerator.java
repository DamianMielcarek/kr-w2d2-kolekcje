package com.szkolenie.kolekcje;

import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class LottoGenerator {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random random = new Random();

        Set<Integer> losowanie = new TreeSet<>();
        Set<Integer> kupon = new TreeSet<>();

        // Losowanie 6 liczb od 1 do 49
        while (losowanie.size() < 6) {
            losowanie.add(random.nextInt(48) + 1);
        }

        // Odczyt kuponu od uzytkownika
        while (kupon.size() < 6) {
            System.out.println("Podaj kolejna liczbe");
            kupon.add(sc.nextInt());
        }
        System.out.println("Kupon: " + kupon);

        // Wyliczenie trafionych liczb !
        kupon.retainAll(losowanie);

        System.out.println("Losowanie: " + losowanie);
        System.out.println(kupon);
    }
}
